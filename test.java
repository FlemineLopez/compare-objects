@author
/*
The following JAVA program demonstrates how to compare the
state of two objects without using comparator
*/

import java.lang.reflect.*;
class sample{
	//method compares two objects of any class and returns true or false
	public static boolean checkEquals(Object o1,Object o2)throws IllegalAccessException  {
	    
		try{
			boolean flag = true;
			
			//checks if both objects are same objects
			
			if (o1== o2) {
				return flag;
			}
			
			//checks if any object is null and if both objects are of same object or not
			
			if (o1 == null ||o2 == null || o1.getClass() != o2.getClass()) {
				return !flag;
			}
			
			//takes all the fields of both objects into field arrays
			
			Field[] f1 = o1.getClass().getDeclaredFields();
			Field[] f2 = o2.getClass().getDeclaredFields();
			
			//checks each field with the same field of both objects
			
			for(int i=0;i<f1.length;i++) {
				if(!(f1[i].get(o1).equals(f2[i].get(o2)))){
					flag = false;
					break;
				}
				else
					flag=true;
			}
			return flag;
		}
		
		//catches exception for illegal access of fields
		
		catch (IllegalAccessException e) { 
            System.out.println("Access restricted!");
			return false;
        } 
	}
}
class test{
   
	
    public static void main(String[] args) {
		A a1=new A(10,20);
		A a2=new A(10,20);
		B b1=new B(10,20,30);
		B b2=null;
		try{
			System.out.println(sample.checkEquals(a1,a2));
			System.out.println(sample.checkEquals(a1,b1));
			System.out.println(sample.checkEquals(a1,a1));
			System.out.println(sample.checkEquals(b2,b1));
		}
		catch (IllegalAccessException e) { 
            System.out.println("Access restricted!");
		}
		catch(NullPointerException e1){
			System.out.println("Object values null");
		}
		
	}
}
//defining two classes A and B of two and three variables
class A
{
	int a,b;
    A(int a,int b){
	  this.a=a;
	  this.b=b;
  }
}

class B
{
	int a,b,c;
	B(int a,int b,int c){
		this.a=a;
		this.b=b;
		this.c=c;
	}
}
